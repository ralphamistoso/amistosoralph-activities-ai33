import Vue from "vue";
import Vuex from "vuex";

import books from "./modules/books.js";
import patrons from "./modules/patrons.js";
import categories from "./modules/categories.js";
import borrowedBook from "./modules/borrowedbook.js";

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		books,
		patrons,
		categories,
		borrowedBook,
	},
});
