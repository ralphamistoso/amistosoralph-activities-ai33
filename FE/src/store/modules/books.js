import axios from "@/assets/js/config/axiosConfig";
const state = {
	books: [],
};

const getters = {
	allBooks: (state) => state.books,
};

const mutations = {
	setBooks: (state, books) => (state.books = books),
	setNewBook: (state, book) => state.books.unshift(book),
	setDeleteBook: (state, id) =>
		(state.books = state.books.filter((book) => book.id !== id)),
	setUpdateBook: (state, updatedBook) => {
		const index = state.books.findIndex((book) => book.id === updatedBook.id);
		if (index !== -1) {
			state.books.splice(index, 1, updatedBook);
		}
	},
};

const actions = {
	async fetchBooks({ commit }) {
		const response = await axios.get("/books");
		commit("setBooks", response.data);
	},

	async createBook({ commit }, book) {
		await axios.post("/books", book);
		commit("setNewBook", result.data);
	},

	async deleteBook({ commit }, id) {
		await axios.delete(`/books/${id}`);
		commit("setDeleteBook", id);
	},

	async updateBook({ commit }, book) {
		var id = book.id;
		var body = {
			name: book.name,
			author: book.author,
			copies: book.copies,
			category_id: book.category_id,
		};

		await axios.put(`/books/${id}`, body);
		commit("setUpdateBook", result.data.book);
	},
};

export default {
	state,
	getters,
	actions,
	mutations,
};
