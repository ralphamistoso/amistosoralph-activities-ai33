import axios from "@/assets/js/config/axiosConfig";
const state = {
	borrowedbooks: [],
};

const getter = {
	allBorrowed: (state) => state.borrowedbooks,
};

const mutations = {
	setBorrowed: (state, borrowedbooks) => (state.borrowedbooks = borrowedbooks),
	setNewBorrow: (state, borrowBook) => state.borrowedbooks.unshift(borrowBook),
};

const actions = {
	async fetchBorrowed({ commit }) {
		const response = await axios.get("/borrowedbook");

		commit("setBorrowed", response.data);
	},

	async borrowBook({ commit }, borrowBook) {
		await axios
			.post("/borrowedbook", borrowBook)
			.then((result) => {
				commit("setNewBorrow", result.data);
			
			})
	},
};

export default {
	state,
	getter,
	mutations,
	actions,
};
