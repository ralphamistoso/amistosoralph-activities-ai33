import axios from "@/assets/js/config/axiosConfig";
const state = {
	patrons: [],
};

const getters = {
	allPatrons: (state) => state.patrons,
};

const mutations = {
	setPatrons: (state, patrons) => (state.patrons = patrons),
	setNewPatron: (state, patron) => state.patrons.unshift(patron),
	setdeletePatron: (state, id) =>
		(state.patrons = state.patrons.filter((patron) => patron.id !== id)),
	setUpdatePatron: (state, updatedPatron) => {
		const index = state.patrons.findIndex(
			(patron) => patron.id === updatedPatron.id
		);
		if (index !== -1) {
			state.patrons.splice(index, 1, updatedPatron);
		}
	},
};

const actions = {
	async fetchPatrons({ commit }) {
		const response = await axios.get("/patrons");

		commit("setPatrons", response.data);
	},

	async deletePatron({ commit }, id) {
		await axios.delete(`/patrons/${id}`);

		commit("setdeletePatron", id);
	},

	async updatePatron({ commit }, patron) {
		var id = patron.id;
		var body = {
			last_name: patron.last_name,
			first_name: patron.first_name,
			middle_name: patron.middle_name,
			email: patron.email,
		};

		await axios.put(`/patrons/${id}`, body).then((result) => {
			commit("setUpdatePatron", result.data.patron);
		});
	},

	async createPatron({ commit }, patron) {
		await axios.post("/patrons", patron).then((result) => {
			commit("setNewPatron", result.data);
		});
	},
};

export default {
	state,
	getters,
	actions,
	mutations,
};
