<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryList= [
            [ 'category' => 'Horror'],
            [ 'category' => 'Fantasy'],
            [ 'category' => 'Romance'],
            [ 'category' => 'History'],
            [ 'category' => 'Science Fiction']
        ];
            foreach ($categoryList as $rows) {
            Category::create($rows);
                }
    }
}
