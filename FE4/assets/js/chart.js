
var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['Philippines', 'Indonesia', 'Australia', 'Japan'],
        datasets: [{
            data: [2500, 2000, 2700, 1300],
            backgroundColor: [
                'rgb(255, 0, 0)',
                'rgb(255, 165, 0)',
                'rgb(51, 204, 255)',
                'rgb(0, 0, 255)'
                
            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Books', 'Computers', 'News Papers', 'Magazines', 'Comics'],
        datasets: [{
            label: 'Inventory',
            data: [500, 90, 275, 305, 178],
            backgroundColor: [
                'rgb(3, 31, 99)',
                'rgb(0, 153, 0)',
                'rgb(3, 31, 99)',
                'rgb(0, 153, 0)',
                'rgb(3, 31, 99)'
                
               

            ],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



